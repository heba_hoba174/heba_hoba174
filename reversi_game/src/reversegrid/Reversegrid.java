package reversegrid;

import java.applet.*;
import java.awt.*;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.xml.soap.Node;

import java.util.Stack;


/**
 *The class extends the applet and implements the runnable to
 * run the applet and use the Graphics
 *
 * @author Ricardo
 * @param <ComputerPlayer>
 */
public class Reversegrid<ComputerPlayer> extends Applet implements Runnable{

    final int Width=280,Empty=0,Black=1,White=-1;
    private final int upper=0,lower=1,right=2,left=3,upperleft=4,upperright=5,
            lowerright=6,lowerleft=7; // directions to check the possible moves
    boolean direction[]={false,false,false,false,false,false,false,false}; // directions set to false to indicate the possible moves
    public int turn;
    public int counter;
    protected int board[][];
    protected int countBlack=0, countWhite=0;
    public int current;
    public boolean playerTurn;
    ComputerPlayer computer; //object from computer player class
    public String welcome = "Reversi Game";
    String title = "Reversi";
    public String ans;
    public JButton b1,b2,b3;

    Stack<Node> list = new Stack<Node>();
    



     /**
     *the init method contains the board object 8x8
      * it will initialize the first four discs it will count the discs
      * and also will set the first turn to the black player
     * @param void
     */
    @Override
    public void init(){

        board = new int[8][8];

        for(int x=0; x<8; x++){
            for(int y=0; y<8; y++){
                board[x][y]=Empty;
            }
        }
        board[3][3]=Black; board[4][3]=White;
        board[3][4]=White; board[4][4]=Black;


        countDisc();
        turn=Black;
    //    computer = new ComputerPlayer(this); //calls the computerPlayer constructor to do the moves

        // will display the gui for the introduction of the game and ask for game mode ans dave the answere
        JOptionPane.showMessageDialog(null, welcome, title, JOptionPane.PLAIN_MESSAGE);
        ans=JOptionPane.showInputDialog(null, "Please select your game mode: 1 - Single Player 2 - Multiplayer");
        
        b1=new JButton("Undo");
        b1.setBounds(200/2-20,300/2-20,40,40);
        b1.setFocusable(false);
        b1.setVisible(true);
        add(b1);

//        b2=new JButton("Single Player");
//        b2.setBounds(200/2-20,300/2-20,40,40);
//        b2.setFocusable(false);
//        b2.setVisible(true);
//        add(b2);
//
//        b3=new JButton("2-Players");
//        b3.setBounds(200/2-20,300/2-20,40,40);
//        b3.setFocusable(false);
//        b3.setVisible(true);
//        add(b3);

    }


    /**
     *The method will draw the board by setting the background color
     * also the color of the lines and using a for loop to divide evenly
     * the board 8x8
     * @param g of type Graphics
     */
    public void drawBoard(Graphics g){

        this.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                test(evt);
            }

            @Override
             public void mousePressed(java.awt.event.MouseEvent evt) {
                test2(evt);
            }

            @Override
              public void mouseReleased(java.awt.event.MouseEvent evt) {
                test3(evt);
            }

        });

            b1.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                test4(evt);
            }
        });

        setBackground(Color.LIGHT_GRAY);
       
 
        g.setColor(Color.black);
        g.drawLine(0, 0, 0, Width);// left line vertical
        g.drawLine(Width, 0, Width, Width);//rigth line vertical
        g.drawLine(0, 0, Width, 0);//top line horizontal
        g.drawLine(0, Width, Width, Width); //bottom line horizontal

        for(int x=1; x<8; x++){
            g.drawLine(Width*x/8, 0, Width*x/8, Width);//draw vertical lines exact 8 columns
            g.drawLine(0,Width*x/8, Width, Width*x/8);//draw horizontal lines by 8
        }
    }
    /**
     *The method will draw the discs setting the color  using the graphics
     * to fill an oval and centering the circle in the square
     *
     * @param col integer
     * @param row integer
     * @param g Graphics
     */
    public void drawDisc(int col, int row, Graphics g){

        if(board[col][row]==Black){
            g.setColor(Color.black);
        }else if(board[col][row]==White){
            g.setColor(Color.white);
        }
        // fills the oval in the cordanatedesired and seeting the widht and height of the oval
        g.fillOval(col*Width/8, row*Width/8,
                Width/8, Width/8);
    }

    public void test(java.awt.event.MouseEvent evt )
    {

        int x = evt.getX();
        int y = evt.getY();


        if (evt.getButton() == 1)                  
        {
            mouseUp(null, x, y);
        }
    }

    public void test2(java.awt.event.MouseEvent evt )
    {

        int x = evt.getX();
        int y = evt.getY();


        if (evt.getButton() == 3)
        {
             for( x=0; x<8; x++)
                for( y=0; y<8; y++)
                   if(checkDisc(x,y,turn)==true) // it will draw the valid moves on the board
                        drawValidDisc(x,y,getGraphics());

        }

    }

    public void test3(java.awt.event.MouseEvent evt )
    {
        repaint();

    }
    
        public void test4(java.awt.event.MouseEvent evt) {

        if (evt.getButton()==1){
            undo(getGraphics());

        }
    }

    @Override
    public void paint(Graphics g){
 
        drawBoard(g);

        for(int x=0; x<8; x++){
            for(int y=0; y<8; y++){
                if(board[x][y] !=0){
                    drawDisc(x,y,g);// draw the discs on the desired place
                }
            }
        }
        drawTurn(g);
        drawCountDisc(g);

        // if the sum of both discs is 64 and the sum not equal 64 show the winner
        if(countBlack + countWhite==64 && countBlack + countWhite!=64 ){
            winner();
        }
    }

    /**
     *The method will draw strings to let us know when is the turn of each player
     * depending of the color turn displaying the color of the string corresponding
     * to the current color playing.
     * @param g Graphics
     */
    public void drawTurn(Graphics g){

        String black="Black";
        String white="White";
        String pTurn="Player's Turn";

        g.setColor(Color.black);

        if(turn==Black){

            g.drawString(black + pTurn, Width/2, Width+35);
            g.setColor(Color.black);
            showStatus("Player 1 Turn");
        }
        else{
            g.drawString(white + pTurn, Width/2, Width+35);
            g.setColor(Color.white);
            showStatus("Player 2 Turn");
        }
        g.fillOval(Width/2-20, Width+20, 20, 20);
        
    }

     /**
     *the method will draw the the counters for the discs just to display
     * the number of discs of each color .
      *
     * @param g Graphics
     */
    public void drawCountDisc(Graphics g){

        g.setColor(Color.white);// sets the color of the oval
        g.fillOval(Width+5, 160, 20, 20); // fills the oval at certain position
        g.setColor(Color.black);// sets the color of the oval
        g.fillOval(Width+5, 100, 20, 20);
        g.drawString("Black", Width+30, 115);
        g.drawString("White", Width+30, 175);
        g.drawString(Integer.toString(countBlack), Width+20, 145);
        g.drawString(Integer.toString(countWhite), Width+20, 205);

  }
    /**
     * The method will draw the ovals of the valid moves and will set the color of the ovals
     * depending on the turn of the current color
     * @param col
     * @param row
     * @param g
     */
    public void drawValidDisc(int col, int row, Graphics g){

        if(board[col][row]==Black){
            g.setColor(Color.RED);
        }else if(board[col][row]==White){
            g.setColor(Color.RED);
        }
        // fills the oval in the cordanatedesired and seeting the widht and height of the oval
        g.setColor(Color.RED);
        g.drawOval(col*Width/8, row*Width/8,
                Width/8, Width/8);
    }

    public void removeValidDisc(int col, int row, Graphics g){

        if(board[col][row]==Black){
            g.setColor(Color.LIGHT_GRAY);
        }else if(board[col][row]==White){
            g.setColor(Color.LIGHT_GRAY);
        }
        // fills the oval in the cordanatedesired and seeting the widht and height of the oval
        g.setColor(Color.LIGHT_GRAY);
        g.drawOval(col*Width/8, row*Width/8,
                Width/8, Width/8);
    }


    /**
     *The method will count the total discs of each color thru out the game
     * when they do the sum of the discs if is the total discs
     * show winner and end game and initialize the game again.
     */
    public void countDisc(){

        countBlack=0;
        countWhite=0;

        for(int x=0; x<8; x++){
            for(int y=0; y<8; y++){
                if(board[x][y]==Black)
                    countBlack++;
                if(board[x][y]==White)
                    countWhite++;
            }
        }

        if(countBlack+countWhite==64){
            winner();
            init();
        }
    }

    /**
     *The method checks for the winner if black has more than white the blacks wins
     * or vice versa otherwise it will be a draw game
     */
    public boolean winner(){

        if(countBlack>countWhite){
            JOptionPane.showMessageDialog(null, "Black Wins");
        }else if(countBlack<countWhite){
            JOptionPane.showMessageDialog(null, "White Wins");
        }else{
            JOptionPane.showMessageDialog(null, "Draw Game");
        }
        repaint(); // it will paint again the game when the game ends
        return true;

    }
    public boolean check(int turn){
        for(int x=0; x<8; x++){
            for(int y=0; y<8; y++){
                if(checkDisc(x,y,turn)){
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean mouseUp(Event e, int x, int y){

        int col=(int)(x/(Width/8));
        int row=(int)(y/(Width/8));

        if(ans.equals("2")){
        // on the turn of the color it will check the discs in all directions
        // if is set to true it will call the method to turn the discs, count the actual discs
        // and get the actual state of the board
            if(turn==Black){

                if(checkDisc(col,row,turn)==true){
                    turnDisc(col,row,turn);
                    turn=-turn;
                    countDisc();
                    copyBoard();
                    update(getGraphics());

                }
            }else if (turn == White) {

                if(checkDisc(col,row,turn)==true){
                    turnDisc(col,row,turn);
                    turn=-turn;
                    countDisc();
                    copyBoard();
                    update(getGraphics());
                }
            }
        }else if(ans.equals("1")) // if the answer is 1 equals single player against cpu
        {
            if(turn==Black){
                if(checkDisc(col,row,turn)==true){
                    turnDisc(col,row,turn);
                    turn=-turn;
                    countDisc();
                    update(getGraphics());
                    copyBoard();
                }
            }
            if (turn == White) {
          //      computer.cpuMove(); // how the computer will move
                countDisc();
                update(getGraphics());
                copyBoard();
            }
        }
        else if(ans.equals("0")){
            init();
        }
        return true;
    }

    /**
     * The method will check for the discs in all directions to found discs of the same
     * color or other color or if the position is empty to check if is a valid move
     * using the numbers of the directions i thought it was easier than using strings for the directions
     *
     * @param col integer
     * @param row integer
     * @param color integer
     * @return boolean
     */
    @SuppressWarnings("empty-statement")
    public boolean checkDisc(int col,int row, int color){

        int x,y;

        for(x=0; x<8; x++){
            direction[x]=false;
        }
        if(board[col][row]!=0){
            return false;
        }
        else{
            if(col>1&&board[col-1][row]==-color){
                for(x=col-2;x>0&&board[x][row]==-color;x--);
                if(board[x][row]==color){
                    direction[left]=true;
                }
            }
            if (col< 6 && board[col+1][row] == -color) {
                 for (x = col+2; x < 7 && board[x][row] == -color; x++);
                 if (board[x][row] == color) {
                    direction[right] = true;
                }
            }
            if (row > 1 && board[col][row-1] == -color) {
                for (y = row-2; y > 0 && board[col][y] == -color; y--);
                if (board[col][y] == color) {
                    direction[upper] = true;
                }
            }
            if (row < 6 && board[col][row+1] == -color) {
                for (y = row+2; y < 7 && board[col][y] == -color; y++);
                if (board[col][y] == color) {
                     direction[lower] = true;
                }
            }
            if (col > 1 && row > 1 && board[col-1][row-1] == -color) {
                for (x = col-2, y = row-2; x > 0 && y > 0 && board[x][y] == -color; x--, y--);
                if (board[x][y] == color) {
                    direction[upperleft] = true;
                }
            }
            if (col < 6 && row > 1 && board[col+1][row-1] == -color) {
                for (x = col+2, y = row-2; x < 7 && y > 0 && board[x][y] == -color; x++, y--);
                if (board[x][y] == color) {
                    direction[upperright] = true;
                }
            }
            if (col < 6 && row < 6 && board[col+1][row+1] == -color) {
                for (x = col+2, y = row+2; x < 7 && y < 7 && board[x][y] == -color; x++, y++);
                if (board[x][y] == color) {
                    direction[lowerright] = true;
                }
            }
            if (col > 1 && row < 6 && board[col-1][row+1] == -color) {
                for (x = col-2, y = row+2; x > 0 && y < 7 && board[x][y] == -color; x--, y++);
                if (board[x][y] == color) {
                    direction[lowerleft] = true;
                }
            }
            for (x = 0; x < 8; x++){
                if (direction[x] == true){
                    return true;
                }
            }
            return false;
        }
    }

    /**
     * if the current color by checking the direction is true it will go check
     * on the directions possible if is not the color and if is not the same as that position
     * it will start turning the discs.
     * @param col integer
     * @param row integer
     * @param color integer
     */
    public void turnDisc(int col, int row, int color) {

    board[col][row] = color;

    int x,y;

    if (direction[left] == true){
      for (x = col-1; board[x][row] != color; x--){
        board[x][row] = - board[x][row];
      }
    }
    if (direction[right] == true){
      for (x = col + 1; board[x][row] != color; x++){
        board[x][row] = - board[x][row];
      }
    }
    if (direction[upper] == true){
      for (y = row - 1; board[col][y] != color; y--){
        board[col][y] = - board[col][y];
      }
    }
    if (direction[lower] == true){
      for (y = row + 1; board[col][y] != color; y++){
        board[col][y] = - board[col][y];
      }
    }
    if (direction[upperleft] == true){
      for (x = col-1, y = row-1; board[x][y] != color; x--, y--){
        board[x][y] = - board[x][y];
      }
    }
    if (direction[upperright] == true){
      for (x = col+1, y = row-1; board[x][y] != color; x++, y--){
        board[x][y] = - board[x][y];
      }
    }
    if (direction[lowerright] == true){
      for (x = col+1, y = row+1; board[x][y] != color; x++, y++){
        board[x][y] = - board[x][y];
      }
    }
    if (direction[lowerleft] == true){
      for (x = col-1, y = row+1; board[x][y] != color; x--, y++){
        board[x][y] = - board[x][y];
      }
    }
  }
    public void copyBoard(){

        int [][]copy=new int[8][8];
        Node tempGame = null;
		//Node tempGame= new Node(copy);
        list.push(tempGame);

        for(int x=0; x<8; x++){
            for(int y=0; y<8; y++){
                copy[x][y]=board[x][y];
            }
        }
    }

    public void undo(Graphics g){

         Node temp =list.pop();
         drawBoard(g);

        for(int x=0; x<8; x++){
            for(int y=0; y<8; y++){
          /*      if(temp.board[x][y] !=0){
                    drawDisc(x,y,g);// draw the discs on the desired place
                }*/
            }
        }
        drawTurn(g);
        drawCountDisc(g);

    }

    public void run() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
